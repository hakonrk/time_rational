/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org/>
 */

#include <stdio.h>

#include "time_rational.hpp"

using namespace std;

static int cnt_failures;
static int cnt_tests;

void failed(char const *s)
{
    fprintf(stderr, "failure #%d: %s\n", cnt_failures, s);
}

#define EXPECT_TRUE(expr) \
    try { \
        ++cnt_tests; \
        if (!(expr)) { \
            ++cnt_failures; \
            failed(#expr); \
        } \
    } catch (...) { \
        ++cnt_failures; \
        failed(#expr " should not throw an exception"); \
    }

#define EXPECT_FALSE(expr) \
    try { \
        ++cnt_tests; \
        if (expr) { \
            ++cnt_failures; \
            failed(#expr); \
        } \
    } catch (...) { \
        ++cnt_failures; \
        failed(#expr " should not throw an exception"); \
    }

#define EXPECT_EXCEPTION(expr) \
    try { \
        ++cnt_tests; \
        (void) (expr); \
        ++cnt_failures; \
        failed(#expr " should throw an exception"); \
    } catch (...) { }

void test_ctors()
{
    EXPECT_FALSE(Time_rational().defined());
    EXPECT_TRUE(Time_rational(1, 2).defined());
    EXPECT_TRUE(Time_rational(0).defined());
}

void test_conversion()
{
    // TEST: Time_rational::to_double()
    EXPECT_TRUE(Time_rational(1, 2).to_double() == 0.5);
    EXPECT_TRUE(Time_rational(0, 2).to_double() == 0.0);
    EXPECT_EXCEPTION(Time_rational().to_double());

    // TEST: Time_rational::str()
    EXPECT_TRUE(Time_rational(4, 6).str() == "4/6");
    EXPECT_TRUE(Time_rational().str() == "(null)");

    // TEST: Time_rational::timeunits().
    EXPECT_TRUE(Time_rational(0).timeunits() == 0);
    EXPECT_TRUE(Time_rational(3, 6).timeunits() == 3);
    EXPECT_EXCEPTION(Time_rational().timeunits());

    // TEST: Time_rational::timescale().
    EXPECT_TRUE(Time_rational(3, 6).timescale() == 6);
    EXPECT_EXCEPTION(Time_rational().timescale());
}

void test_relops()
{
    // Undefined on the left.
    EXPECT_EXCEPTION(Time_rational() == Time_rational(1, 2));
    EXPECT_EXCEPTION(Time_rational() != Time_rational(1, 2));
    EXPECT_EXCEPTION(Time_rational() < Time_rational(1, 2));
    EXPECT_EXCEPTION(Time_rational() <= Time_rational(1, 2));
    EXPECT_EXCEPTION(Time_rational() >= Time_rational(1, 2));
    EXPECT_EXCEPTION(Time_rational() > Time_rational(1, 2));

    // Undefined on the right.
    EXPECT_EXCEPTION(Time_rational(1, 2) == Time_rational());
    EXPECT_EXCEPTION(Time_rational(1, 2) != Time_rational());
    EXPECT_EXCEPTION(Time_rational(1, 2) < Time_rational());
    EXPECT_EXCEPTION(Time_rational(1, 2) <= Time_rational());
    EXPECT_EXCEPTION(Time_rational(1, 2) >= Time_rational());
    EXPECT_EXCEPTION(Time_rational(1, 2) > Time_rational());

    // Zero numerators, unequal denominators.
    EXPECT_TRUE(Time_rational(0, 1) == Time_rational(0, 2));
    EXPECT_FALSE(Time_rational(0, 1) != Time_rational(0, 2));
    EXPECT_FALSE(Time_rational(0, 1) < Time_rational(0, 2));
    EXPECT_TRUE(Time_rational(0, 1) <= Time_rational(0, 2));
    EXPECT_TRUE(Time_rational(0, 1) >= Time_rational(0, 2));
    EXPECT_FALSE(Time_rational(0, 1) > Time_rational(0, 2));

    // Equal fractions with different denominators.
    EXPECT_TRUE(Time_rational(1, 2) == Time_rational(2, 4));
    EXPECT_FALSE(Time_rational(1, 2) != Time_rational(2, 4));
    EXPECT_FALSE(Time_rational(1, 2) < Time_rational(2, 4));
    EXPECT_TRUE(Time_rational(1, 2) <= Time_rational(2, 4));
    EXPECT_FALSE(Time_rational(1, 2) > Time_rational(2, 4));
    EXPECT_TRUE(Time_rational(1, 2) >= Time_rational(2, 4));

    // Unequal fractions, left side bigger.
    EXPECT_FALSE(Time_rational(1, 2) == Time_rational(1, 3));
    EXPECT_TRUE(Time_rational(1, 2) != Time_rational(1, 3));
    EXPECT_FALSE(Time_rational(1, 2) < Time_rational(1, 3));
    EXPECT_FALSE(Time_rational(1, 2) <= Time_rational(1, 3));
    EXPECT_TRUE(Time_rational(1, 2) >= Time_rational(1, 3));
    EXPECT_TRUE(Time_rational(1, 2) > Time_rational(1, 3));

    // Unequal fractions, right side bigger.
    EXPECT_FALSE(Time_rational(1, 3) == Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(1, 3) != Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(1, 3) < Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(1, 3) <= Time_rational(1, 2));
    EXPECT_FALSE(Time_rational(1, 3) >= Time_rational(1, 2));
    EXPECT_FALSE(Time_rational(1, 3) > Time_rational(1, 2));

    // Test floating point conversion.
    EXPECT_TRUE(Time_rational(0.5) == Time_rational(1, 2));
}

void test_assignops()
{
    Time_rational a, b, c;

    a = Time_rational(1, 2);
    a += a;
    EXPECT_TRUE(a == Time_rational(1, 1));
    a -= a;
    EXPECT_TRUE(a == Time_rational(0, 1));

    a = Time_rational(1, 2);
    b = Time_rational(1, 3);
    c = a;
    c += b;
    EXPECT_TRUE(c == Time_rational(1.0/2.0 + 1.0/3.0));
    c = Time_rational(0, 6);
    c -= Time_rational(1, 6);
    EXPECT_TRUE(c == -Time_rational(1, 6));

    a = Time_rational(2, 4);
    a += Time_rational(4, 4);
    EXPECT_TRUE(a.timeunits() == 6 && a.timescale() == 4);
    a -= Time_rational(2, 4);
    EXPECT_TRUE(a.timeunits() == 4 && a.timescale() == 4);

    a = Time_rational(1, 1);
    b = a;
    b += Time_rational(1, 2);
    EXPECT_TRUE(a == Time_rational(1, 1) && b == Time_rational(3, 2));
}

void test_addsub()
{
    // Unary + operator.
    EXPECT_TRUE(Time_rational(1, 2) == +Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(0, 1) == +Time_rational(0, 1));
    EXPECT_TRUE((+Time_rational(2, 4)).timeunits() == 2);
    EXPECT_EXCEPTION(+Time_rational());

    // Unary - operator.
    EXPECT_FALSE(Time_rational(1, 2) == -Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(0, 1) == -Time_rational(0, 1));
    EXPECT_TRUE((-Time_rational(2, 4)).timescale() == 4);
    EXPECT_TRUE((-Time_rational(1, 2)).str() == "-1/2");
    EXPECT_EXCEPTION(-Time_rational());

    // Binary + operator.
    EXPECT_TRUE(Time_rational(0, 1) + Time_rational(2, 4) == Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(1, 2) + (-Time_rational(2, 4)) == Time_rational(0, 1));
    EXPECT_EXCEPTION(Time_rational(1, 2) + Time_rational());
    EXPECT_EXCEPTION(Time_rational() + Time_rational(1, 2));
    EXPECT_TRUE((Time_rational(1, 2) + Time_rational(1, 2)).timeunits() == 2);

    // Binary - operator.
    EXPECT_TRUE(Time_rational(0, 1) - Time_rational(2, 4) == -Time_rational(1, 2));
    EXPECT_TRUE(Time_rational(1, 2) - Time_rational(2, 4) == Time_rational(0, 1));
    EXPECT_EXCEPTION(Time_rational(1, 2) - Time_rational());
    EXPECT_EXCEPTION(Time_rational() - Time_rational(1, 2));
    EXPECT_TRUE((Time_rational(4, 4) - Time_rational(2, 4)).timeunits() == 2);
}

int main()
{
    test_ctors();
    test_conversion();
    test_relops();
    test_addsub();
    test_assignops();
    printf("%d tests performed, %d failures\n", cnt_tests, cnt_failures);
    return 0;
}
