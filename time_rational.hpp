/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org/>
 */

#ifndef TIME_RATIONAL_HPP
#define TIME_RATIONAL_HPP

#include <inttypes.h>
#include <string>

class Time_rational {
public:
    /*
     * Creates an uninitialized Time_rational (Time_rational::defined() will
     * return false for objects created with this default constructor).
     */
    Time_rational();

    /*
     * Creates an independent copy of the given Time_rational.
     */
    Time_rational(Time_rational const &);

    /*
     * Create a Time_rational based on the given values.  timeunits is the number
     * of timeunits, and timescale is the number of timeunits per seconds.  In
     * other words, timeunits/timescale = time in seconds.
     *
     * Note that we use unsigned numbers (to get the maximum range of the
     * number).  A constructor for negative numbers is not required for our
     * purposes.
     */
    Time_rational(int64_t timeunits, uint32_t timescale);

    /*
     * Convert a floating point number to a rational number, with some
     * reasonable timescale.
     */
    Time_rational(double sec);

    ~Time_rational();

    /*
     * Convert the time value from a rational arbitrary precision number to
     * seconds expressed as a double-precision floating pointer number.
     */
    double to_double() const;

    /*
     * Convert the time value to a C string (written a fraction, VAL/TIMESCALE).
     */
    std::string str() const;

    /*
     * Return just the numerator part.
     */
    int64_t timeunits() const;

    /*
     * Return just the denominator part.
     */
    uint32_t timescale() const;

    /*
     * Returns false if no value has yet been set.
     */
    bool defined() const;

    Time_rational& operator=(Time_rational const &);
    Time_rational& operator+=(Time_rational const &);
    Time_rational& operator-=(Time_rational const &);

    friend void sanity_check_unary_operator_arg(Time_rational const &a);
    friend void sanity_check_binary_operator_args(Time_rational const &a, Time_rational const &b);
    friend bool operator==(Time_rational const &, Time_rational const &);
    friend bool operator!=(Time_rational const &, Time_rational const &);
    friend Time_rational operator+(Time_rational const &, Time_rational const &);
    friend Time_rational operator-(Time_rational const &, Time_rational const &);
    friend Time_rational operator+(Time_rational const &);
    friend Time_rational operator-(Time_rational const &);
    friend bool operator<(Time_rational const &, Time_rational const &);
    friend bool operator>(Time_rational const &, Time_rational const &);
    friend bool operator<=(Time_rational const &, Time_rational const &);
    friend bool operator>=(Time_rational const &, Time_rational const &);

private:
    int64_t m_timeunits;
    uint32_t m_timescale;

    // If a number with a different timescale has been added, we stop using
    // rational numbers and switch to floating point.  We don't care about
    // exact representations if that happens.
    double m_sec;

    double to_double_nocheck() const;
};

#endif /* TIME_RATIONAL_HPP */
