.DEFAULT_GOAL := all

AR := ar
CC := clang
CXX := clang++

COMMON_CFLAGS := -std=c99 -g -W -Wall
COMMON_CXXFLAGS := -std=c++11 -g -W -Wall

COMMON_CPPFLAGS := -I.
COMMON_LDFLAGS :=
COMMON_LIBS :=

all_targets :=
all_sources :=
all_deps :=
all_objects :=

define begin_target =
target :=
sources :=
CPPFLAGS :=
CFLAGS :=
CXXFLAGS :=
LDFLAGS :=
LIBS :=
endef

define end_target =
all_targets += $(target)
all_sources += $(sources)
all_objects += $(patsubst %.c,%.o,$(filter %.c,$(sources))) $(patsubst %.cpp,%.o,$(filter %.cpp,$(sources)))
all_deps += $(patsubst %.c,%.d,$(filter %.c,$(sources))) $(patsubst %.cpp,%.d,$(filter %.cpp,$(sources)))
$(target): $(patsubst %.c,%.o,$(filter %.c,$(sources))) $(patsubst %.cpp,%.o,$(filter %.cpp,$(sources))) $(LIBS)
ifneq (,$(filter %.a,$(target)))
	$(AR) rcs $(target) $(patsubst %.c,%.o,$(filter %.c,$(sources))) $(patsubst %.cpp,%.o,$(filter %.cpp,$(sources)))
else
ifneq (,$(filter %.cpp,$(sources)))
	$(CXX) $(COMMON_LDFLAGS) $(LDFLAGS) $(patsubst %.c,%.o,$(filter %.c,$(sources))) $(patsubst %.cpp,%.o,$(filter %.cpp,$(sources))) $(LIBS) $(COMMON_LIBS) -o $(target)
else
	$(CC) $(COMMON_LDFLAGS) $(LDFLAGS) $(patsubst %.c,%.o,$(filter %.c,$(sources))) $(patsubst %.cpp,%.o,$(filter %.cpp,$(sources))) $(LIBS) $(COMMON_LIBS) -o $(target)
endif
endif

$(patsubst %.c,%.d,$(filter %.c,$(sources))): %.d: %.c
	@echo "Finding dependencies for $$(<:.c=.o)."
	@set -e; $(CC) -M $(COMMON_CPPFLAGS) $(CPPFLAGS) $(COMMON_CFLAGS) $(CFLAGS) $$< \
		| sed 's,^\(.*\.o\): ,\1 $$@: ,' > $$@ \
		; [ -s $$@ ] || rm -f $$@

$(patsubst %.cpp,%.d,$(filter %.cpp,$(sources))): %.d: %.cpp
	@echo "Finding dependencies for $$(<:.cpp=.o)."
	@set -e; $(CC) -M $(COMMON_CPPFLAGS) $(CPPFLAGS) $(COMMON_CXXFLAGS) $(CXXFLAGS) $$< \
		| sed 's,^\(.*\.o\): ,\1 $$@: ,' > $$@ \
		; [ -s $$@ ] || rm -f $$@

$(patsubst %.c,%.o,$(filter %.c,$(sources))): %.o: %.c
	$(CC) -c $(COMMON_CPPFLAGS) $(CPPFLAGS) $(COMMON_CFLAGS) $(CFLAGS) $$< -o $$@

$(patsubst %.cpp,%.o,$(filter %.cpp,$(sources))): %.o: %.cpp
	$(CXX) -c $(COMMON_CPPFLAGS) $(CPPFLAGS) $(COMMON_CXXFLAGS) $(CXXFLAGS) $$< -o $$@
endef

.PHONY: all dep dist clean distclean
.SUFFIXES:

$(eval $(begin_target))
target := test
sources := test.cpp
LIBS := time_rational.a
$(eval $(end_target))

$(eval $(begin_target))
target := time_rational.a
sources := time_rational.cpp
$(eval $(end_target))

all:
	@$(MAKE) all_inner use_deps=1

all_inner: $(all_targets)

%.h:;

ifeq ($(use_deps),1)
-include $(all_deps)
endif

tags:
	ctags *

dep:

dist: distclean

clean:
	rm -f $(all_targets) $(all_objects)

distclean: clean
	rm -f $(all_deps) core gmon.out tags cscope.out
