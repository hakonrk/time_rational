/*
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org/>
 */

#ifndef _GNU_SOURCE
# define _GNU_SOURCE 1 // for strchrnul()
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdexcept>
#include <sstream>
#include "time_rational.hpp"

using namespace std;

void sanity_check_unary_operator_arg(Time_rational const &a)
{
    if (!a.defined()) {
        throw domain_error("invalid use of undefined Time_rational");
    }
}

void sanity_check_binary_operator_args(Time_rational const &a, Time_rational const &b)
{
    if (!a.defined() || !b.defined()) {
        throw domain_error("invalid use of undefined Time_rational");;
    }
}

Time_rational::Time_rational()
    : m_timeunits(0)
    , m_timescale(0)
    , m_sec(NAN)
{
}

Time_rational::Time_rational(Time_rational const &other)
    : m_timeunits(other.m_timeunits)
    , m_timescale(other.m_timescale)
    , m_sec(other.m_sec)
{
}

Time_rational::Time_rational(int64_t _timeunits, uint32_t _timescale)
    : m_timeunits(_timeunits)
    , m_timescale(_timescale)
    , m_sec(0)
{
}

Time_rational::Time_rational(double _sec)
    : m_timeunits(0)
    , m_timescale(0)
    , m_sec(_sec)
{
}

static string to_str(long long timeunits, unsigned timescale)
{
    char buf[4096];
    snprintf(buf, sizeof(buf), "%lld/%u", timeunits, timescale);
    return string(buf);
}

static string to_str(double d)
{
    stringstream ss;
    ss << d;
    return ss.str();
}

Time_rational::~Time_rational()
{
}

bool Time_rational::defined() const
{
    return m_timescale != 0 || !isnan(m_sec);
}

double Time_rational::to_double() const
{
    sanity_check_unary_operator_arg(*this);
    return to_double_nocheck();
}

double Time_rational::to_double_nocheck() const
{
    return m_timescale == 0 ? m_sec : m_timeunits/double(m_timescale);
}

string Time_rational::str() const
{
    return !defined() ? "(null)"
         : m_timescale != 0 ? to_str(m_timeunits, m_timescale)
         :                  to_str(m_sec);
}

int64_t Time_rational::timeunits() const
{
    sanity_check_unary_operator_arg(*this);
    return m_timeunits;
}

uint32_t Time_rational::timescale() const
{
    sanity_check_unary_operator_arg(*this);
    return m_timescale;
}

Time_rational& Time_rational::operator=(Time_rational const &other)
{
    if (this != &other) {
        sanity_check_unary_operator_arg(other);
        m_timeunits = other.m_timeunits;
        m_timescale = other.m_timescale;
        m_sec = other.m_sec;
    }
    return *this;
}

#define DEFINE_OPASSIGN(OP) \
    Time_rational& Time_rational::operator OP (Time_rational const &other) \
    { \
        sanity_check_binary_operator_args(*this, other); \
        if (m_timescale == other.m_timescale) { \
            if (m_timescale != 0) { \
                m_timeunits OP other.m_timeunits; \
            } else { \
                m_sec OP other.m_sec; \
            } \
        } else { \
            if (m_timescale != 0) { \
                m_sec = m_timeunits/double(m_timescale); \
                m_timeunits = m_timescale = 0; \
            } \
            m_sec OP other.to_double_nocheck(); \
        } \
        return *this; \
    }

DEFINE_OPASSIGN(+=)
DEFINE_OPASSIGN(-=)

#define DEFINE_OP(OP) \
    Time_rational operator OP (Time_rational const &a, Time_rational const &b) \
    { \
        sanity_check_binary_operator_args(a, b); \
        Time_rational ret; \
        if (a.m_timescale == b.m_timescale) { \
            if (a.m_timescale != 0) { \
                ret.m_timeunits = a.m_timeunits OP b.m_timeunits; \
                ret.m_timescale = a.m_timescale; \
            } else { \
                ret.m_sec = a.m_sec OP b.m_sec; \
            } \
        } else { \
            ret.m_sec = a.to_double_nocheck() OP b.to_double_nocheck(); \
        } \
        return ret; \
    }

DEFINE_OP(+)
DEFINE_OP(-)

Time_rational operator+(Time_rational const &a)
{
    sanity_check_unary_operator_arg(a);
    return a;
}

Time_rational operator-(Time_rational const &a)
{
    sanity_check_unary_operator_arg(a);
    Time_rational ret(a);
    if (a.m_timescale) {
        ret.m_timeunits *= -1;
    } else {
        ret.m_sec *= -1.0;
    }
    return ret;
}

#define DEFINE_CMP(CMP) \
    bool operator CMP (Time_rational const &a, Time_rational const &b) \
    { \
        sanity_check_binary_operator_args(a, b); \
        if (a.m_timescale != b.m_timescale) { \
            return a.to_double_nocheck() CMP b.to_double_nocheck(); \
        } else { \
            if (a.m_timescale == 0) { \
                return a.m_sec CMP b.m_sec; \
            } else { \
                return a.m_timeunits CMP b.m_timeunits; \
            } \
        } \
    }

DEFINE_CMP(==)
DEFINE_CMP(!=)
DEFINE_CMP(<)
DEFINE_CMP(<=)
DEFINE_CMP(>)
DEFINE_CMP(>=)
